DESCRIPTION = "Customized image for Udoo board with JavaFX"

require recipes-udoo/images/udoo-image-full-cmdline.bb

UDOO_EXTRA_INSTALL += " \
    pango \
    fontconfig \
    openssh-sftp-server \ 
    ntp \
"
